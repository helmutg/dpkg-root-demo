#!/bin/sh
# run this script with debbisect like this:
#
# debbisect --cache=./cache --depends="fakechroot fakeroot devscripts mmdebstrap python3 reprepro sbuild schroot diffoscope git ca-certificates" --qemu=defaults "6 days ago" today ./debbisect.sh

set -exu

ssh -F "$1" qemu env --chdir=/tmp git clone https://salsa.debian.org/helmutg/dpkg-root-demo.git
ssh -F "$1" qemu sbuild-createchroot --debootstrap=mmdebstrap --alias=sid unstable /srv/chroot/unstable-sbuild $DEBIAN_BISECT_MIRROR
ssh -F "$1" qemu adduser --gecos build --disabled-password build
ssh -F "$1" qemu adduser build sbuild
ssh -F "$1" qemu chown build -R /tmp/dpkg-root-demo
ssh -F "$1" qemu env --chdir=/tmp/dpkg-root-demo MIRROR=$DEBIAN_BISECT_MIRROR runuser build -c ./update.sh
ssh -F "$1" qemu env --chdir=/tmp/dpkg-root-demo MIRROR=$DEBIAN_BISECT_MIRROR runuser build -c ./install.sh
