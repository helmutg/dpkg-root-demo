#!/bin/sh
# SPDX-License-Identifier: MIT
# /bin/sh is either bash or dash:
# shellcheck disable=SC2039

set -u
set -e

. ./setup.sh

BUILD_ARCH=$(dpkg --print-architecture)
HOST_ARCH=${1:-$BUILD_ARCH}

if ! test -d "$REPREPRO_BASE_DIR"; then
	mkdir -p "$REPREPRO_BASE_DIR/conf"
	cat > "$REPREPRO_BASE_DIR/conf/distributions" <<EOF
Codename: $OURSUITE
Label: $OURSUITE
Architectures: $HOST_ARCH $(test "$BUILD_ARCH" = "$HOST_ARCH" || echo "$BUILD_ARCH")
Components: main
UDebComponents: main
Description: updated packages for DPKG_ROOT
EOF
	cat > "$REPREPRO_BASE_DIR/conf/options" <<EOF
verbose
ignore wrongdistribution
EOF
	reprepro export
fi

chdistdata=$(pwd)/chdist
chdist_base() {
	local cmd
	cmd=$1
	shift
	chdist "--data-dir=$chdistdata" "$cmd" base "$@"
}
if [ ! -d "$chdistdata" ]; then
	chdist_base create
fi
cat << END > "$chdistdata/base/etc/apt/sources.list"
deb-src $MIRROR $BASESUITE main
END
chdist_base apt-get update

todo=
for p in patches/*; do
	p=${p#patches/}

	if [ ! -e "patches/$p" ]; then
		echo "patches/$p doesn't exist"
		continue
	fi

	if [ ! -x "patches/$p" ]; then
		echo "patches/$p is not executable"
		continue
	fi

	# shellcheck disable=SC2016
	our_version=$(reprepro --list-format '${version}_${source}\n' -T deb listfilter "$OURSUITE" "\$Source (== $p)" | sed 's/.*_.*(\(.*\))$/\1/;s/_.*//' | uniq)
	their_version=$(chdist_base apt-get source --only-source -t "$BASESUITE" --no-act "$p" | sed "s/^Selected version '\\([^']*\\)' ($BASESUITE) for .*/\\1/;t;d")
	if test -z "$their_version"; then
		echo "cannot determine source version for $p"
		exit 1
	fi
	if test -n "$our_version" && dpkg --compare-versions "$our_version" gt "$their_version"; then
		if [ -e repo/dists/$OURSUITE/Release ] && [ repo/dists/$OURSUITE/Release -ot "patches/$p" ]; then
			echo "patches/$p has been changed -- rebuilding"
		else
			echo "package $p up to date"
			continue
		fi
	fi

	todo="$todo $p"
done

# Since this script can also be run locally outside of a CI, we add a number
# of options to overwrite possible settings in the local ~/.sbuildrc.
#
# The --apt-upgrade and --apt-distupgrade options are necessary to make sure
# that patched packages from the essential and build-essential set are
# installed in their most recent version in the build chroot
COMMON_SBUILD_OPTS="-d $BASESUITE --nolog --no-clean-source --no-source-only-changes --no-run-lintian --no-run-autopkgtest --apt-upgrade --apt-distupgrade"
COMMON_BUILD_PROFILES="nobiarch,nocheck,noudeb"

for p in $todo; do
	reprepro removesrc "$OURSUITE" "$p"

	rm -Rf "$WORKDIR"
	mkdir "$WORKDIR"
	(
		cd "$WORKDIR" 
		chdist_base apt-get source --only-source -t "$BASESUITE" "$p"
		cd "$p-"*
		"$PATCHDIR/$p"
		faketime @$SOURCE_DATE_EPOCH dch --local "+$OURSUITE" "apply DPKG_ROOT patch"
		faketime @$SOURCE_DATE_EPOCH dch --release ""
		version=$(dpkg-parsechangelog --show-field Version | sed 's/^[0-9]\+://')
		archanypkgs=$(env DEB_HOST_ARCH="$HOST_ARCH" DEB_BUILD_PROFILES="cross $(echo $COMMON_BUILD_PROFILES | tr ',' ' ')" dh_listpackages -a)

		if test "$BUILD_ARCH" != "$HOST_ARCH" && [ -n "$archanypkgs" ]; then
			# build foreign architecture arch:any packages if needed
			# shellcheck disable=SC2086
			DEB_BUILD_OPTIONS="noautodbgsym nocheck noudeb" \
				sbuild --host="$HOST_ARCH" \
					--no-arch-all --arch-any \
					--profiles="cross,$COMMON_BUILD_PROFILES" \
					$COMMON_SBUILD_OPTS \
					--extra-repository="$SRC_LIST_PATCHED"
		fi
		nativepkgs=$(env DEB_HOST_ARCH="$BUILD_ARCH" DEB_BUILD_PROFILES="$(echo $COMMON_BUILD_PROFILES | tr ',' ' ')" dh_listpackages)
		# build arch:all and arch:any packages natively
		# shellcheck disable=SC2086
		if [ -n "$nativepkgs" ]; then
			DEB_BUILD_OPTIONS="noautodbgsym nocheck noudeb" \
				sbuild --arch-all --arch-any \
					--profiles="$COMMON_BUILD_PROFILES" \
					$COMMON_SBUILD_OPTS \
					--extra-repository="$SRC_LIST_PATCHED"
		fi
		cd ..

		# We only include the changes file into the repository after
		# both the foreign and native build are done because:
		#  - binaries will only be included if both builds succeeded
		#    and prevent situations in which a failing native build is
		#    not re-attempted.
		#  - if the second build fails, it will not leave the
		#    repository with a M-A:same version skew.
		reprepro include "$OURSUITE" "./${p}_${version}_${BUILD_ARCH}.changes"
		if test "$BUILD_ARCH" != "$HOST_ARCH" && [ -n "$archanypkgs" ]; then
			reprepro include "$OURSUITE" "./${p}_${version}_${HOST_ARCH}.changes"
		fi
	)
	rm -Rf "$WORKDIR"
done

for p in $(reprepro --list-format '${source}\n' -T deb list "$OURSUITE" | sed 's/^\([^ (]\+\).*/\1/' | sort -u); do
	if [ ! -e "patches/$p" ]; then
		echo "patches/$p doesn't exist -- removing from repo"
		reprepro removesrc "$OURSUITE" "$p"
		continue
	fi

	if [ ! -x "patches/$p" ]; then
		echo "patches/$p is not executable --removing from repo"
		reprepro removesrc "$OURSUITE" "$p"
		continue
	fi
done
